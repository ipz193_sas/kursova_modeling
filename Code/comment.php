<?php

declare(strict_types=1);


class comment
{

    /** @var int */
    private int $comment_id;

    /** @var int */
    private int $user_id;

    /** @var int */
    private int $lesson_id;

    /** @var string */
    public string $text;

    /** @var date */
    public date $created_at;

    /** @var int */
    private int $status;

    /**
     * Default constructor
     */
    public function __construct()
    {
        // ...
    }

}
