<?php

declare(strict_types=1);


class payment_check
{

    /** @var int */
    private int $payment_id;

    /** @var int */
    private int $user_id;

    /** @var int */
    private int $course_id;

    /** @var int */
    private int $student_id;

    /** @var int */
    protected int $status_id;

    /** @var string */
    public string $payment_link;

    /** @var int */
    public int $full_price;

    /** @var int */
    public int $discount;

    /** @var int */
    public int $final_price;

    /**
     * Default constructor
     */
    public function __construct()
    {
        // ...
    }

    /**
     * 
     */
    public function isCoursePaid()
    {
        // TODO implement here
    }

}
