<?php

declare(strict_types=1);


class user
{

    /** @var int */
    private int $user_id;

    /** @var string */
    public string $username;

    /** @var string */
    public string $firstname;

    /** @var string */
    public string $lastname;

    /** @var varchar */
    protected varchar $password_hash;

    /** @var varchar */
    protected varchar $password_reset_token;

    /** @var string */
    public string $email;

    /** @var string */
    public string $image;

    /**
     * Default constructor
     */
    public function __construct()
    {
        // ...
    }

    /**
     * 
     */
    public function addComment()
    {
        // TODO implement here
    }

    /**
     * 
     */
    public function deleteComment()
    {
        // TODO implement here
    }

}
