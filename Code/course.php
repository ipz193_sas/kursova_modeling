<?php

declare(strict_types=1);


class course
{

    /** @var int */
    private int $course_id;

    /** @var string */
    public string $course_name;

    /** @var int */
    private int $owner_id;

    /** @var DateTime */
    public DateTime $created_at;

    /** @var DateTime */
    public DateTime $updated_at;

    /**
     * Default constructor
     */
    public function __construct()
    {
        // ...
    }

}
