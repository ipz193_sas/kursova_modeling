<?php

declare(strict_types=1);


class progress
{

    /** @var int */
    private int $progress_id;

    /** @var int */
    private int $user_id;

    /** @var int */
    private int $lesson_id;

    /** @var int */
    private int $status_id;

    /** @var DateTime */
    protected DateTime $date_status;

    /**
     * Default constructor
     */
    public function __construct()
    {
        // ...
    }

}
