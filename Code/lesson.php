<?php

declare(strict_types=1);


class lesson
{

    /** @var int */
    private int $lesson_id;

    /** @var int */
    private int $course_id;

    /** @var string */
    public string $title;

    /** @var text */
    public text $description;

    /** @var text */
    public text $text;

    /** @var string */
    public string $link_video;

    /** @var date */
    public date $created_at;

    /** @var date */
    public date $updated_at;

    /**
     * Default constructor
     */
    public function __construct()
    {
        // ...
    }

    /**
     * 
     */
    public function isLessonOpened()
    {
        // TODO implement here
    }

    /**
     * 
     */
    public function isLessonPassed()
    {
        // TODO implement here
    }

}
